class FacilitiesController < ApplicationController
  
  def index
    @facilities = Facility.all
  end
  
  def show
    @facility = Facility.find(params[:id])
  end
  
  def edit
    
  end
  
  def new
    
  end
  
  def create
    
  end
  
  def update
    
  end
  
  def destroy
    
  end
  
end
