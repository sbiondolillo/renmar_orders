class EquipmentsController < ApplicationController
  
  def index
    @equipments = Equipment.all
  end
  
  def show
    @equipment = Equipment.find(params[:id])
  end
  
  def edit
    
  end
  
  def new
    
  end
  
  def create
    
  end
  
  def update
    
  end
  
  def destroy
    
  end
  
end
