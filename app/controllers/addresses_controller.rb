class AddressesController < ApplicationController
  
  def index
    @addresses = Address.all
  end
  
  def show
    @address = Address.find(params[:id])
  end
  
  def edit
    
  end
  
  def new
    
  end
  
  def create
    
  end
  
  def update
    
  end
  
  def destroy
    
  end
  
end
