class CustomerRepsController < ApplicationController
  
  def index
    @customer_reps = CustomerRep.all
  end
  
  def show
    @customer_rep = CustomerRep.find(params[:id])
  end
  
  def edit
    
  end
  
  def new
    
  end
  
  def create
    
  end
  
  def update
    
  end
  
  def destroy
    
  end
  
end
