class Order < ActiveRecord::Base
  belongs_to :customer
  belongs_to :customer_rep
  belongs_to :client
  belongs_to :facility
  belongs_to :employee
  has_many   :equipments
  has_one    :address, :through => :facility
end
