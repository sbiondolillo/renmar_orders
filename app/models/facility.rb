class Facility < ActiveRecord::Base
  has_many :clients
  has_many :orders
  has_many :equiments
  has_one  :address, :as => :owner
end
