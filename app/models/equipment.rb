class Equipment < ActiveRecord::Base
  belongs_to :customer
  belongs_to :client
  belongs_to :facility
  belongs_to :order
  has_one    :address, :through => :facility
end
