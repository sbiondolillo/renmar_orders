class Customer < ActiveRecord::Base
  has_many :customer_reps
  has_many :clients
  has_many :orders
  has_many :equipments
  has_one  :address, :as => :owner
end
