class Client < ActiveRecord::Base
  belongs_to :customer
  belongs_to :facility
  has_many   :equipments
  has_many   :orders
  has_one    :address,  :through => :facility
end
