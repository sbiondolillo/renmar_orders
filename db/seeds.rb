# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

facility1 = Facility.create([{name: "Concord Office", classification: "Renmar"}])
add1 = Address.create([{address_1: "3 Ormond St", address_2: nil, city: "Concord", state: "NH", zip: 03301, owner_type: "Facility", owner_id: 1}])
add2 = Address.create([{address_1: "119 Old Trunpike Rd", address_2: nil, city: "Concord", state: "NH", zip: 03301, owner_type: "Customer", owner_id: 1}])
employee1 = Employee.create([{first_name: "Sam", last_name: "Thomas", title: "Distribution Manager"}])
customer1 = Customer.create([{name: "Test Customer"}])
cust_rep1 = CustomerRep.create([{first_name: "Jane", last_name: "Doe", title: "Office Manager", customer_id: 1}])
client1 = Client.create([{first_name: "Jon", last_name: "Doe", dob: "01/01/1930", facility_id: 1, customer_id: 1}])
equip1 = Equipment.create([{make: "Broda", model: "Midline", asset_num: "1234", classification: "Wheelchair", customer_id: 1, client_id: 1, facility_id: 1, order_id: 1}])
order1 = Order.create([{classification: "Delivery", priority: "High", date_placed: Time.now, date_completed: nil, customer_id: 1, client_id: 1, customer_rep_id: 1, employee_id: 1, facility_id: 1}])