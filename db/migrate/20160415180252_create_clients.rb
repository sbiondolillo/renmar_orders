class CreateClients < ActiveRecord::Migration
  def change
    create_table :clients do |t|
      t.string :first_name
      t.string :last_name
      t.string :dob
      t.integer :facility_id
      t.integer :customer_id
      t.timestamps null: false
    end
  end
end
