class CreateEquipment < ActiveRecord::Migration
  def change
    create_table :equipment do |t|
      t.string :make
      t.string :model
      t.string :asset_num
      t.string :classification
      t.integer :customer_id
      t.integer :client_id
      t.integer :facility_id
      t.integer :order_id
      t.timestamps null: false
    end
  end
end
