class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.string :classification
      t.string :priority
      t.datetime :date_placed
      t.datetime :date_completed
      t.integer :customer_id
      t.integer :client_id
      t.integer :customer_rep_id
      t.integer :employee_id
      t.integer :facility_id
      t.timestamps null: false
    end
  end
end
