class CreateCustomerReps < ActiveRecord::Migration
  def change
    create_table :customer_reps do |t|
      t.string :first_name
      t.string :last_name
      t.string :title
      t.integer :customer_id
      t.timestamps null: false
    end
  end
end
